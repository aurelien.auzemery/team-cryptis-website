# Site web Team CRYPTIS

## Introduction

Ce repository git contient le code source du [site web de l'association Team CRYPTIS](https://teamcryptis.fr). Le site utilise [Hugo](https://gohugo.io) avec le thème [Stack](https://stack.jimmycai.com/), ce qui permet à n'importe qui de contribuer facilement au blog en écrivant les articles en Markdown.

## Pour contribuer

Toute contribution est la bienvenue, en soumettant une Merge Request (MR). Il est seulement nécessaire d'installer Hugo, git, et un éditeur de code.

Pour cloner le dépôt:

- `git clone git@gitlab.com:team-cryptis/team-cryptis-website.git` (il est recommandé d'utiliser des clés SSH pour git)
- `git submodule update --init` (initialise le thème Stack qui est un submodule git)

Hugo inclut un serveur de développement local avec la commande `hugo server`. Il n'y a pas besoin de compiler le site pour tester des versions de développement ou pour soumettre une nouvelle version: le repository GitLab publie automatiquement sur le site en production les nouvelles mises à jour, une fois celles-ci fusionnée dans `main`.

Les documentations suivantes sont utiles pour se familiariser avec Hugo et le thème utilisé:

- [Hugo - Documentation](https://gohugo.io/documentation/)
- [Stack theme - Documentation](https://stack.jimmycai.com/guide/)
